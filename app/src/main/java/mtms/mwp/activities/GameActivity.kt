package mtms.mwp.activities

import android.app.Activity
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import mtms.mwp.R
import mtms.mwp.utils.GameState
import mtms.mwp.utils.PointsStatus
import mtms.mwp.utils.Session
import kotlinx.android.synthetic.main.activity_game.*
import mtms.mwp.Upgrade
import mtms.mwp.UpgradesListAdapter
import mtms.mwp.Winner
import java.lang.StringBuilder
import java.math.BigDecimal
import java.math.RoundingMode

class GameActivity : Activity() {
    private lateinit var animation: AnimationDrawable
    private val maxpoints: BigDecimal = BigDecimal(10000)

    private var leverLevel: BigDecimal = BigDecimal(1)
    private val leverPrice: BigDecimal = BigDecimal(200)

    private var time: BigDecimal = BigDecimal(0)

    private lateinit var adapter: UpgradesListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        if (intent.getBooleanExtra("continue", false)) {
            val gamestate = Session.loadGameState(this)
            PointsStatus.points = gamestate.score
            leverLevel = gamestate.leverLevel
            upgradeLeverImage(gamestate.leverLevel.toInt()) {
                setAnimation(it)
            }
            Session.setTime(gamestate.time)
            if (leverLevel.toInt() == 3) {
                this.leverLevelLabel.visibility = View.INVISIBLE
                this.leverPriceLabel.visibility = View.INVISIBLE
                this.upgradeLever.visibility = View.INVISIBLE
            }
        } else {
            PointsStatus.points = BigDecimal(0)
            Session.setTime(BigDecimal(0))
            setAnimation(R.drawable.pull_lever)
        }

        PointsStatus.onUpdate {
            runOnUiThread {
                playAnimation()
                updatePoints()
            }
        }

        setupUpgrades()
        Upgrade.setOnUpgrade { setupUpgrades() }
        updatePoints()
        updateLeverUpgrade()
        if (Session.isTimePaused) {
            Session.startTime { updateTime(it) }
        }
        if (Upgrade.getUpgrades()[0].getLevel() >= BigDecimal(1) && PointsStatus.isAutoClickerPaused) {
            PointsStatus.enableAutoClicker()
        }
    }

    override fun onPause() {
        super.onPause()

        if (!Session.loadSettings(this).continueMinimised) {
            Session.pauseTime()
            PointsStatus.stopAutoClicker()
        }
    }

    override fun onResume() {
        super.onResume()

        if (!Session.loadSettings(this).continueMinimised) {
            if (Session.isTimePaused) {
                Session.startTime { updateTime(it) }
            }
            if (Upgrade.getUpgrades()[0].getLevel() >= BigDecimal(1) && PointsStatus.isAutoClickerPaused) {
                PointsStatus.enableAutoClicker()
            }
        }
    }

    override fun onBackPressed() {}

    override fun onDestroy() {
        super.onDestroy()
        Session.pauseTime()
        Session.saveUpgrades(this)
        PointsStatus.stopAutoClicker()
        if (PointsStatus.points < this.maxpoints) {
            saveGameState()
        }
    }

    @Suppress("UNUSED_PARAMETER")
    fun onBackClicked(view: View) {
        Session.pauseTime()
        Session.saveUpgrades(this)
        PointsStatus.stopAutoClicker()
        saveGameState()
        finish()
    }

    @Suppress("UNUSED_PARAMETER")
    fun onLeverClicked(view: View) {
        playAnimation()
        PointsStatus.addPoints(leverLevel)
        updatePoints()
        if (PointsStatus.points >= maxpoints) {
            finishGame()
        }
    }

    fun onLeverUpgradeClicked(view: View) {
        val upgradePrice = this.leverLevel.times(this.leverPrice)
        if (PointsStatus.points >= upgradePrice) {
            leverLevel++
            updateLeverUpgrade()
            PointsStatus.subtractPoints(upgradePrice)
            upgradeLeverImage(this.leverLevel.toInt()) {
                setAnimation(it)
            }
            updatePoints()

            if (leverLevel.toInt() == 3) {
                this.leverLevelLabel.visibility = View.INVISIBLE
                this.leverPriceLabel.visibility = View.INVISIBLE
                view.visibility = View.INVISIBLE
            }
        }
    }

    @Suppress("UNUSED_PARAMETER")
    fun onUpgradesClicked(view: View) {
        this.upgradesList.visibility = if (this.upgradesList.visibility == View.VISIBLE) View.INVISIBLE else View.VISIBLE
    }

    @Suppress("UNUSED_PARAMETER")
    fun onUpgradesListClicked(view: View) {
        updatePoints()
    }

    private fun upgradeLeverImage(level: Int, handler: (Int) -> Unit) {
        when(level) {
            1 -> handler(R.drawable.pull_lever)
            2 -> handler(R.drawable.pull_lever_2)
            3 -> handler(R.drawable.pull_lever_3)
        }
    }

    private fun updateLeverUpgrade() {
        val leverPrice = this.leverPrice * this.leverLevel
        this.leverLevelLabel.text = StringBuilder("lvl ").append(leverLevel).toString()
        this.leverPriceLabel.text = if (this.leverLevel.toInt() != 3) StringBuilder(leverPrice.toString()).append(" punktów").toString() else "MAX"
    }

    private fun setAnimation(drawableId: Int) {
        this.gameAnimation.setBackgroundResource(drawableId)
        animation = this.gameAnimation.background as AnimationDrawable
    }

    private fun playAnimation() {
        animation.stop()
        animation.start()
    }

    private fun updatePoints() {
        if (PointsStatus.points >= maxpoints) {
            finishGame()
        }
        this.gameScore.text = StringBuilder(PointsStatus.points.toString())
            .append("/")
            .append(maxpoints.toString())
            .toString()
    }

    private fun finishGame() {
        this.gameAnimation.isClickable = false
        val name = Session.loadSettings(this).playerName
        Session.addWinner(Winner(name, this.time))
        Session.saveWinners(this)
        Session.clearGameState(this)
        finish()
    }

    private fun updateTime(time: BigDecimal) {
        this.time = time
        runOnUiThread {
            this.timePassing.text = ("Time: ${time.setScale(1, RoundingMode.UP).toDouble()}")
        }
    }

    private fun saveGameState() {
        Session.saveGameState(
            this,
            GameState(
                PointsStatus.points,
                this.leverLevel,
                Session.getTime()
            )
        )
    }

    private fun setupUpgrades() {
        val upgrades = Upgrade.getUpgrades()
        adapter = UpgradesListAdapter(ArrayList(upgrades))
        this.upgradesList.layoutManager =
            LinearLayoutManager(this)
        this.upgradesList.adapter = adapter
    }
}