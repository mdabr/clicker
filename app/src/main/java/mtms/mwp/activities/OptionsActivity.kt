package mtms.mwp.activities

import android.app.Activity
import android.os.Bundle
import android.view.View
import mtms.mwp.utils.BackgroundAnimation
import mtms.mwp.R
import mtms.mwp.utils.Session
import mtms.mwp.utils.Settings
import kotlinx.android.synthetic.main.activity_options.*

class OptionsActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_options)
        BackgroundAnimation(this.optionsBackground, R.drawable.options).start()
    }

    override fun onStart() {
        super.onStart()
        val settings = Session.loadSettings(this)
        initSettingsView(settings)
    }

    override fun onBackPressed() {}

    @Suppress("UNUSED_PARAMETER")
    fun onBackClicked(view: View) {
        val settings = getSettingsFromView()
        Session.saveSettings(this, settings)
        finish()
    }

    private fun initSettingsView(settings: Settings) {
        this.playerNameInput.setText(settings.playerName)
        this.minimisedContinue.isChecked = settings.continueMinimised
        this.useless.isChecked = settings.useless
        this.doesNothing.isChecked = settings.doesNothing
        this.whyDidIEven.isChecked = settings.whyDidIEven
    }

    private fun getSettingsFromView() : Settings {
        return Settings(
            this.playerNameInput.text.toString(),
            this.minimisedContinue.isChecked,
            this.useless.isChecked,
            this.doesNothing.isChecked,
            this.whyDidIEven.isChecked
        )
    }
}