package mtms.mwp.activities

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import mtms.mwp.utils.BackgroundAnimation
import mtms.mwp.R
import mtms.mwp.Upgrade
import kotlinx.android.synthetic.main.activity_main.*
import mtms.mwp.utils.Session

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        startBackgroundAnimation()
        Session.loadWinners(this)
        verifyContinue()
    }

    override fun onBackPressed() {}

    override fun onResume() {
        super.onResume()

        this.newGame.isEnabled = true
        this.options.isEnabled = true
        this.ranking.isEnabled = true
        this.continueGame.isEnabled = true

        verifyContinue()
    }

    fun onNewGameClicked(view: View) {
        view.isEnabled = false
        Session.stopTime()
        Upgrade.resetUpgrades()
        startActivity(
            Intent(this, GameActivity::class.java)
                .putExtra("continue", false)
        )
    }

    fun onOptionsClicked(view: View) {
        view.isEnabled = false
        startActivity(
            Intent(this, OptionsActivity::class.java)
        )
    }

    fun onRankingClicked(view: View) {
        view.isEnabled = false
        startActivity(
            Intent(this, RankingActivity::class.java)
        )
    }

    fun onContinueClicked(view: View) {
        view.isEnabled = false
        Session.loadUpgrades(this)
        startActivity(
            Intent(this, GameActivity::class.java)
                .putExtra("continue", true)
        )
    }

    @Suppress("UNUSED_PARAMETER")
    fun onExitClicked(view: View) {
        finish()
    }

    private fun startBackgroundAnimation() {
        BackgroundAnimation(this.mainBackground, R.drawable.main_menu).start()
    }

    private fun verifyContinue() {
        val preferences: SharedPreferences = Session.getPreferences(this)
        this.continueGame.visibility = if (preferences.contains("score")) View.VISIBLE else View.INVISIBLE
    }
}
