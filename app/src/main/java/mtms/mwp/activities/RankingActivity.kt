package mtms.mwp.activities

import android.app.Activity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import mtms.mwp.R
import mtms.mwp.WinnersListAdapter
import mtms.mwp.utils.Session
import kotlinx.android.synthetic.main.activity_ranking.*

class RankingActivity : Activity() {
    private lateinit var adapter: WinnersListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ranking)

        val winners = Session.getWinners()
        adapter = WinnersListAdapter(ArrayList(winners))
        this.rankingList.layoutManager =
            LinearLayoutManager(this)
        this.rankingList.adapter = adapter
    }

    override fun onBackPressed() {}

    @Suppress("UNUSED_PARAMETER")
    fun onBackClicked(view: View) {
        finish()
    }
}