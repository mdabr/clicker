package mtms.mwp

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.winner_item.view.*

class WinnerHolder(private var view: View): RecyclerView.ViewHolder(view), View.OnClickListener {
    private var winner: Winner? = null

    companion object {
        private val WINNER_KEY = "WINNER"
    }

    init {
        view.setOnClickListener(this)
    }

    override fun onClick(view: View) {}

    fun bindWinner(winner: Winner, index: Int) {
        this.winner = winner
        val winnerName = "${index+1}. ${winner.name}"
        view.winner_name.text = winnerName
        view.winner_time.text = winner.time.toString() // TODO: add a time formatter
    }
}