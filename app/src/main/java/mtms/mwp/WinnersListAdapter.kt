package mtms.mwp

import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import mtms.mwp.utils.inflate

class WinnersListAdapter(private val winners: ArrayList<Winner>) : RecyclerView.Adapter<WinnerHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WinnerHolder {
        val inflatedView = parent.inflate(R.layout.winner_item, false)
        return WinnerHolder(inflatedView)
    }

    override fun getItemCount(): Int {
        return winners.size
    }

    override fun onBindViewHolder(holder: WinnerHolder, position: Int) {
        val itemWinner = winners[position]
        holder.bindWinner(itemWinner, position)
    }

}