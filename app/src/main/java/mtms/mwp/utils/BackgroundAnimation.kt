package mtms.mwp.utils

import android.graphics.drawable.AnimationDrawable
import android.widget.ImageView

class BackgroundAnimation(image: ImageView, drawableId: Int) {
    private var animation: AnimationDrawable

    init {
        image.setBackgroundResource(drawableId)
        this.animation = image.background as AnimationDrawable
    }

    fun start() {
        animation.start()
    }
}