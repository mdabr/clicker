package mtms.mwp.utils

import mtms.mwp.Upgrade
import java.math.BigDecimal
import java.util.*
import kotlin.concurrent.timer

class PointsStatus {
    companion object {
        var isAutoClickerPaused: Boolean = true
        var points: BigDecimal = BigDecimal(0)
        lateinit var onUpdateCallback: () -> Unit
        var timerObject: Timer? = null
        var autoClickerEnabled = false

        fun addPoints(pointsAdded: BigDecimal) {
            points += pointsAdded
            onUpdateCallback()
        }

        fun subtractPoints(pointsSubtracted: BigDecimal) {
            points -= pointsSubtracted
            onUpdateCallback()
        }

        fun onUpdate(function: () -> Unit) {
            onUpdateCallback = function
        }

        fun enableAutoClicker() {
            autoClickerEnabled = true
            startAutoClicker()
        }

        fun startAutoClicker() {
            if (autoClickerEnabled) {
                val timeUnit = (BigDecimal(1000) / Upgrade.getUpgrades()[0].getLevel()).toLong()
                timerObject = timer("Timer", false, 0, timeUnit) {
                    addPoints(getPointsFromUpgrades())
                }
                isAutoClickerPaused = false
            }
        }

        fun stopAutoClicker() {
            timerObject?.cancel()
            isAutoClickerPaused = true
        }

        private fun getPointsFromUpgrades() : BigDecimal {
            return Upgrade.getUpgrades().fold(BigDecimal(0)) {
                acc, next -> acc.add(next.getPoints())
            }
        }
    }
}