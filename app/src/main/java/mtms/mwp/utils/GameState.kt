package mtms.mwp.utils

import android.content.SharedPreferences
import java.math.BigDecimal

class GameState(var score: BigDecimal = BigDecimal(0), var leverLevel: BigDecimal = BigDecimal(1), var time: BigDecimal = BigDecimal(0)) {
    companion object {
        fun load(preferences: SharedPreferences): GameState {
            return GameState(
                BigDecimal(preferences.getLong("score", 0)),
                BigDecimal(preferences.getLong("leverLevel", 1)),
                BigDecimal(preferences.getLong("time", 0))
            )
        }
    }

    fun save(editor: SharedPreferences.Editor) {
        editor.putLong("score", score.toLong())
        editor.putLong("leverLevel", leverLevel.toLong())
        editor.putLong("time", time.toLong())
    }
}