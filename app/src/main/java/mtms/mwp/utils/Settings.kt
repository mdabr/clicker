package mtms.mwp.utils

data class Settings(var playerName: String) {
    var continueMinimised: Boolean = false
    var useless: Boolean = false
    var doesNothing: Boolean = false
    var whyDidIEven: Boolean = false

    constructor(playerName: String, continueMinimised: Boolean, useless: Boolean, doesNothing: Boolean, whyDidIEven: Boolean) : this(playerName) {
        this.continueMinimised = continueMinimised
        this.useless = useless
        this.doesNothing = doesNothing
        this.whyDidIEven = whyDidIEven
    }
}