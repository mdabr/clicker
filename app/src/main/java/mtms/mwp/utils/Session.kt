package mtms.mwp.utils

import android.content.Context
import android.content.SharedPreferences
import mtms.mwp.R
import mtms.mwp.Upgrade
import mtms.mwp.Winner
import java.math.BigDecimal
import java.util.*
import kotlin.concurrent.timer

class Session {
    companion object {
        private val timeUnit: BigDecimal = BigDecimal(100)
        var timerObject: Timer? = null
        private var time: BigDecimal = BigDecimal(0)
        private var winners: MutableList<Winner> = mutableListOf()
        var isTimePaused = true

        fun saveSettings(context: Context, settings: Settings) {
            val preferences: SharedPreferences = getPreferences(context)
            val editor: SharedPreferences.Editor = preferences.edit()

            editor.putString("playerName", settings.playerName)
            editor.putBoolean("continueMinimised", settings.continueMinimised)
            editor.putBoolean("useless", settings.useless)
            editor.putBoolean("doesNothing", settings.doesNothing)
            editor.putBoolean("whyDidIEven", settings.whyDidIEven)

            editor.apply()
        }

        fun loadWinners(context: Context) {
            val preferences = getPreferences(context)
            val winnersStringified = preferences.getString("winners", "")

            if (winnersStringified?.isEmpty() == true) {
                return
            }

            val winnersStrings = winnersStringified?.split(";")?.filter { it.isNotEmpty() }
            winners = mutableListOf()
            winnersStrings?.forEach {
                winnersString -> winners.add(
                    Winner(
                        winnersString.split(":")[0],
                        BigDecimal(winnersString.split(":")[1])
                    )
                )
            }
        }

        fun saveWinners(context: Context) {
            val preferences = getPreferences(context)
            val editor = preferences.edit()
            val winnersStringified = winners.joinToString(";") { winner -> "${winner.name}:${winner.time}" }
            editor.putString("winners", winnersStringified)
            editor.apply()
        }

        fun addWinner(winner: Winner) {
            winners.sortBy { it.time }
            if (winners.size < 10) {
                winners.add(winner)
            } else {
                if (winner.time < winners[9].time) {
                    winners[9] = winner
                }
            }
            winners.sortBy { it.time }
        }

        @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
        fun loadSettings(context: Context) : Settings {
            val preferences: SharedPreferences = getPreferences(context)
            return Settings(
                preferences.getString("playerName", "Mateusz").toString(),
                preferences.getBoolean("continueMinimised", false),
                preferences.getBoolean("useless", false),
                preferences.getBoolean("doesNothing", false),
                preferences.getBoolean("whyDidIEven", false)
            )
        }

        fun saveGameState(context: Context, gameState: GameState) {
            val preferences: SharedPreferences = getPreferences(context)
            val editor: SharedPreferences.Editor = preferences.edit()

            gameState.save(editor)

            editor.apply()
        }

        fun loadGameState(context: Context) : GameState {
            val preferences: SharedPreferences = getPreferences(context)
            return GameState.load(preferences)
        }

        fun setTime(time: BigDecimal) {
            this.time = time
        }

        fun getTime(): BigDecimal {
            return this.time
        }

        fun stopTime() {
            pauseTime()
            time = BigDecimal(0)
        }

        fun startTime(callback: (BigDecimal) -> Unit) {
            timerObject = timer("Timer", false, 0, timeUnit.toLong()) {
                time = time.add(timeUnit.divide(BigDecimal(1000)))
                callback(time)
            }
            isTimePaused = false
        }

        fun pauseTime() {
            timerObject?.cancel()
            isTimePaused = true
        }

        fun getPreferences(context: Context) : SharedPreferences {
            val preferencesName: String = context.getString(R.string.PREFERENCE_NAME)
            return context.getSharedPreferences(preferencesName, Context.MODE_PRIVATE)
        }

        fun getWinners(): MutableList<Winner> {
            return winners
        }

        fun clearGameState(context: Context) {
            val preferences: SharedPreferences = getPreferences(context)
            val editor: SharedPreferences.Editor = preferences.edit()

            editor.remove("score")
            editor.remove("leverLevel")
            editor.remove("time")
            println(preferences.contains("score"))

            editor.apply()
        }

        fun saveUpgrades(context: Context) {
            val preferences = getPreferences(context)
            val editor = preferences.edit()

            editor.putString("upgrades", Upgrade.stringifiedUpgradesList())
            editor.apply()
        }

        fun loadUpgrades(context: Context) {
            val preferences = getPreferences(context)
            val upgradesString = preferences.getString("upgrades", "").toString()

            if (!upgradesString.contains(":")) {
                return
            }

            upgradesString.split(";").forEachIndexed {
                index: Int, s: String ->  Upgrade.getUpgrades()[index].upgradeLevel = BigDecimal(s.split(":")[1])
            }
        }
    }
}