package mtms.mwp

import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import mtms.mwp.utils.inflate

class UpgradesListAdapter(private val upgrades: ArrayList<Upgrade>) : RecyclerView.Adapter<UpgradeHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UpgradeHolder {
        val inflatedView = parent.inflate(R.layout.upgrade_item, false)
        return UpgradeHolder(inflatedView)
    }

    override fun getItemCount(): Int {
        return upgrades.size
    }

    override fun onBindViewHolder(holder: UpgradeHolder, position: Int) {
        val itemUpgrade = upgrades[position]
        holder.bindUpgrade(itemUpgrade)
        holder.updateView()
    }

}