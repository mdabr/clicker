package mtms.mwp

import mtms.mwp.utils.PointsStatus
import java.math.BigDecimal

class Upgrade(private var name: String, private var baseValue: BigDecimal, private val modifier: BigDecimal, private val parentIndex: Int?) {
    var upgradeLevel: BigDecimal = BigDecimal(0)

    companion object {
        val maxLevel: BigDecimal = BigDecimal(5)
        lateinit var onUpgradeClicked: () -> Unit

        private var upgradesList: List<Upgrade> = listOf(
            Upgrade("Pamięć mięśniowa", BigDecimal(100), BigDecimal(1.5), null),
            Upgrade("Bicek", BigDecimal(200), BigDecimal(2.0), 0),
            Upgrade("Mokasyny rozboju", BigDecimal(300), BigDecimal(2.5), 1),
            Upgrade("Kołczan prawilności", BigDecimal(400), BigDecimal(3.0), 2),
            Upgrade("Czapka antykonfidenta", BigDecimal(500), BigDecimal(3.5), 3),
            Upgrade("Okowy grzmotu", BigDecimal(600), BigDecimal(4.0), 4),
            Upgrade("Żupan popłochu", BigDecimal(700), BigDecimal(4.5), 5)
        )

        fun getUpgrades() : List<Upgrade> {
            return upgradesList
        }

        fun setOnUpgrade(callback: () -> Unit) {
            onUpgradeClicked = callback
        }

        fun stringifiedUpgradesList() : String = upgradesList.joinToString(";") { upgrade -> upgrade.stringify() }
        fun resetUpgrades() {
            upgradesList.forEach { it.upgradeLevel = BigDecimal(0) }
        }
    }

    fun upgrade() {
        if (this.upgradeLevel >= maxLevel || (parentIndex != null && getUpgrades()[parentIndex].getLevel() == BigDecimal(0))) {
            return
        }
        this.upgradeLevel = this.upgradeLevel.inc()
        onUpgradeClicked()
        if (this == getUpgrades()[0] && this.upgradeLevel >= BigDecimal(1)) {
            PointsStatus.stopAutoClicker()
            PointsStatus.enableAutoClicker()
        }
    }

    fun getPoints() : BigDecimal = this.upgradeLevel.times(baseValue.divide(BigDecimal(100))).times(modifier)

    fun getUpgradeName() : String = this.name

    fun getLevel() : BigDecimal = this.upgradeLevel

    fun getPrice() : BigDecimal = if (this.upgradeLevel >= maxLevel) BigDecimal.ZERO else this.upgradeLevel.inc().times(this.baseValue)

    fun stringify() : String = "${this.name}:${this.upgradeLevel}"

    fun getParentIndex(): Int? = this.parentIndex
}