package mtms.mwp

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import mtms.mwp.utils.PointsStatus
import kotlinx.android.synthetic.main.upgrade_item.view.*
import java.math.BigDecimal

class UpgradeHolder(view: View): RecyclerView.ViewHolder(view), View.OnClickListener {
    private var view: View = view
    private lateinit var upgrade: Upgrade

    companion object {
        private val UPGRADE_KEY = "UPGRADE"
    }

    init {
        view.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        if (PointsStatus.points >= this.upgrade.getPrice()) {
            PointsStatus.subtractPoints(this.upgrade.getPrice())
            this.upgrade.upgrade()
            updateView()
        }
    }

    fun updateView() {
        val upgradeParent = if (this.upgrade.getParentIndex() != null) Upgrade.getUpgrades()[this.upgrade.getParentIndex() ?: 0] else null

        view.upgradeName.text = this.upgrade.getUpgradeName()
        view.upgradeLevel.text = "${this.upgrade.getLevel()}/${Upgrade.maxLevel}"
        view.upgradePrice.text = if(this.upgrade.getLevel() >= Upgrade.maxLevel) "MAX" else this.upgrade.getPrice().toString()
        view.visibility = if (upgradeParent != null && upgradeParent.getLevel() == BigDecimal(0)) View.INVISIBLE else View.VISIBLE
    }

    fun bindUpgrade(itemUpgrade: Upgrade) {
        this.upgrade = itemUpgrade
        view.upgradeName.text = itemUpgrade.getUpgradeName()
        view.upgradeLevel.text = "${itemUpgrade.getLevel()}/${Upgrade.maxLevel}"
        view.upgradePrice.text = if(itemUpgrade.getLevel() >= Upgrade.maxLevel) "MAX" else itemUpgrade.getPrice().toString()
    }
}